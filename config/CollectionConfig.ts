import CollectionConfigInterface from '../lib/CollectionConfigInterface';
import { ethereumTestnet, ethereumMainnet } from '../lib/Networks';
import { openSea } from '../lib/Marketplaces';
import whitelistAddresses from './whitelist.json';

const CollectionConfig: CollectionConfigInterface = {
  testnet: ethereumTestnet,
  mainnet: ethereumMainnet,
  // The contract name can be updated using the following command:
  // yarn rename-contract NEW_CONTRACT_NAME
  // Please DO NOT change it manually!
  contractName: 'VikitaNftToken',
  tokenName: 'Vikita NFT Token',
  tokenSymbol: 'VIK',
  hiddenMetadataUri: 'ipfs://QmWJPygR9TXdmXvycwLg1jk5cp6uJxRnzHmPunNxtm8JT3/hidden.json',
  maxSupply: 10000,
  whitelistSale: {
    price: 0.05,
    maxMintAmountPerTx: 1000,
  },
  preSale: {
    price: 0.06,
    maxMintAmountPerTx: 2,
  },
  publicSale: {
    price: 0.07,
    maxMintAmountPerTx: 1,
  },
  contractAddress: '0x6d4F2f75c71AC1e0a0D08e4dEf0906396964E050',
  marketplaceIdentifier: 'vikita',
  marketplaceConfig: openSea,
  whitelistAddresses: whitelistAddresses,
};

export default CollectionConfig;
